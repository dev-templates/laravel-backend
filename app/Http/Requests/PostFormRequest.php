<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $postId = '';

        if (isset($this->post->id)) {
            $postId = $this->post->id;
        }

        return [
            'url' => 'required|unique:posts,url,'.$postId,
            'title' => 'required',
            'name' => 'required',
            'content' => 'required',
        ];
    }
}

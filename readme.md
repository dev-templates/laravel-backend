# Laravel-Backend


A clone of laravel 5.7 to be used for workshops, beginner guides, etc.

# Downloading 

Download by cloning [gitlab/dev-templates/laravel-backend](https://gitlab.com/dev-templates/laravel-backend).

`git clone https://gitlab.com/dev-templates/laravel-backend`


#  Permission denied? "Project does not exist"?

CLone my github instead  (laravel + vue) from my [github.com/jpalala/dev-templates](https://github.com/jpalala/dev-templates)


# Cloning from Gitlab (because you want to contribute, I guess?):

I noticed some issues from gitlab where cloning via git+ssh gives some errors.

You may need to supply your ssh key. To do so,  create a gitlab account, and upload your ssh public key. 

You may also need to add the following to your `~/.ssh/config` file (usually your pub key is id_rsa.pub - don't
forget to change this below).

```
Host gitlab.com
    PubkeyAcceptedKeyTypes +ssh-rsa
    HostName gitlab.com
    IdentityFile ~/.ssh/*your_public_key*
    User git
```

This is to solve the "[The project you were looking for could not be found.](https://gitlab.com/gitlab-com/support-forum/issues/638)" issue.

# How to run

1. Get your env file `cp .env.example .env`

2. Generate your key `php artisan key:generate`

3. PROFIT!
## Generate a controller

`php artisan make:controller HelloController`

## Generate a form

```php

{!! Form::open(['url' => 'foo/bar']) !!}
    //
{!! Form::close() !!}

```



## Dont forget csrf field

`{{ csrf_field }}`

## Setup the submission handler

```
public function show(Request $request)  {
   print_r($request->input());
   echo $request->input('todo');
}
```

## Do Eloquent

[TODO](todo.com)

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

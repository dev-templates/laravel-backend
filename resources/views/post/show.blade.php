@extends('layouts.app')

@section('content')

  <div class="container">

    <h2>{{ $post->title }}</h2>

    <div>
      Posted by {{ $post->name }}
      <time class="timeago" datetime="{{ $post->updated_at->toIso8601String() }}"
            title="{{ $post->updated_at->toDayDateTimeString() }}">
        {{ $post->updated_at->diffForHumans() }}
      </time>
    </div>

    <hr>

    <div>
      {!! $post->content !!}
    </div>

  </div>

@endsection
